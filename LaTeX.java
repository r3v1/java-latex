package com.packModelo;

import de.nixosoft.jlr.JLRConverter;
import de.nixosoft.jlr.JLRGenerator;
import org.apache.velocity.exception.ResourceNotFoundException;

import java.awt.Desktop;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Vector;

public class LaTeX {
    private static LaTeX miLatex = null;

    private File workingDirectory = new File(System.getProperty("user.dir"));
    private File tempDir = new File(workingDirectory + File.separator + "temp");
    private File recetaTex, menuTex;
    private File desktop = new File(System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "RecetasApp");

    private String template = "";
    private JLRConverter converter;

    private LaTeX() {
        if (!tempDir.isDirectory()) {
            tempDir.mkdir();
        }

        if (!desktop.isDirectory()) {
            desktop.mkdir();
        }
    }

    public static LaTeX getMiLatex() {
        if (miLatex == null) {
            miLatex = new LaTeX();
        }

        return miLatex;
    }

    /**
     * Genera el pdf a partir de un .tex
     * <p>
     * http://www.nixo-soft.de/tutorials/jlr/JLRTutorial.html
     *
     * Acceder a un archivo dentro del jar: https://stackoverflow.com/a/20389418
     */
    public void prepararPlantillaReceta(Vector<Object> pInformacion) {
        if (pInformacion != null) {
            String[] parts = String.valueOf(pInformacion.get(0)).split(" ");
            String nombreArchivo;
            if (parts.length >= 3) {
                String part1 = parts[0];
                String part2 = parts[1];
                String part3 = parts[2];
                nombreArchivo = part1 + "_" + part2 + "_" + part3;
            }
            else{
                nombreArchivo = (String) pInformacion.get(0);
            }

            String linea;
            StringBuilder template = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/plantillaReceta.tex")));
            try {
                while ((linea = br.readLine()) != null) {
                    template.append(linea).append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            JLRGenerator pdfGen = new JLRGenerator();
            JLRConverter converter = new JLRConverter(workingDirectory);

            recetaTex = new File(tempDir + File.separator + nombreArchivo + ".tex");

            try {
                if (!Files.exists(Paths.get(desktop.getPath() + File.separator + nombreArchivo + ".pdf"))) {

                    converter.replace("Receta", pInformacion.get(0));
                    converter.replace("Kilos", pInformacion.get(1));
                    converter.replace("Raciones", pInformacion.get(2));
                    converter.replace("PrecioRacion", pInformacion.get(3));
                    converter.replace("PrecioKilo", pInformacion.get(4));
                    if (Files.exists(Paths.get((String) pInformacion.get(5)))){
                        converter.replace("RutaFoto", pInformacion.get(5));
                    }
                    else{
                        converter.replace("RutaFoto", "");
                    }
                    converter.replace("Elaboracion", pInformacion.get(6));
                    converter.replace("Conservacion", pInformacion.get(7));
                    converter.replace("Regeneracion", pInformacion.get(8));

                    Vector<Vector<String>> listaIngredientes = (Vector<Vector<String>>) pInformacion.lastElement();
                    Vector<String> ingredientes = new Vector<>();
                    Vector<String> precios = new Vector<>();
                    Vector<String> totales = new Vector<>();

                    for (Vector<String> listaIngrediente : listaIngredientes) {
                        ingredientes.add(listaIngrediente.get(1) + " " + listaIngrediente.get(4).toLowerCase() + " de " + listaIngrediente.get(0));
                        precios.add(listaIngrediente.get(2));
                        totales.add(listaIngrediente.get(3));
                    }

                    converter.replace("Ingredientes", ingredientes);
                    converter.replace("Precios", precios);
                    converter.replace("Totales", totales);

                    converter.parse(template.toString(), recetaTex);

                    pdfGen.generate(recetaTex, desktop, workingDirectory);
                    File pdf1 = pdfGen.getPDF();

                    abrirPdf(pdf1.getAbsolutePath());
                } else {
                    abrirPdf(desktop.getPath() + File.separator + nombreArchivo + ".pdf");
                }

            } catch (FileNotFoundException | NullPointerException e) {
                System.out.println("No existe el pdf " + desktop.getPath() + File.separator + nombreArchivo + ".pdf");
            } catch (IOException | ResourceNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void prepararPlantillaMenu(Vector <Object> pInformacion) {
        if (pInformacion != null) {
            String nombreArchivo = String.valueOf(pInformacion.get(1)).replace(" ", "_");

            template = this.leerPlantilla("/plantillaMenu.tex");

            JLRGenerator pdfGen = new JLRGenerator();
            this.converter = new JLRConverter(workingDirectory);

            menuTex = new File(tempDir + File.separator + nombreArchivo + ".tex");

            try {
                if (!Files.exists(Paths.get(desktop.getPath() + File.separator + nombreArchivo + ".pdffffffffffffffffffffffffffffff"))) {

                    // Aperitivos
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(3), "Aperitivos");

                    // Entrantes
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(4), "Entrantes");

                    // Pescados
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(5), "Pescados");

                    // Carnes
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(6), "Carnes");

                    // Postres
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(7), "Postres");

                    // Cafes
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(8), "Cafes");

                    // Vinos
                    this.parsearMenu((Vector <Vector <String>>) pInformacion.get(9), "Vinos");

                    converter.replace("PrecioTotal", pInformacion.get(0));
                    converter.replace("NombreMenu", pInformacion.get(1));
                    converter.replace("TipoMenu", pInformacion.get(2));

                    converter.parse(template, menuTex);

                    pdfGen.generate(menuTex, desktop, workingDirectory);
                    File pdf1 = pdfGen.getPDF();

                    abrirPdf(pdf1.getAbsolutePath());
                } else {
                    abrirPdf(desktop.getPath() + File.separator + nombreArchivo + ".pdf");
                }

            } catch (FileNotFoundException | NullPointerException e) {
                System.out.println("No existe el pdf " + desktop.getPath() + File.separator + nombreArchivo + ".pdf");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void abrirPdf(String pRutaPdf) {
        if (Desktop.isDesktopSupported()) {
            try {
                File myFile = new File(pRutaPdf);
                Desktop.getDesktop().open(myFile);
            } catch (IOException | IllegalArgumentException e) {
                System.out.println("No se puede abrir, no existe el pdf " + pRutaPdf);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String leerPlantilla(Object pRuta) {
        String linea;
        StringBuilder template = new StringBuilder();
        BufferedReader br = null;

        try {
            if (pRuta instanceof String) {
                br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream((String) pRuta)));
            } else {
                if (pRuta instanceof File) {
                    br = new BufferedReader(new FileReader((File) pRuta));
                }
            }

            while ((linea = br.readLine()) != null) {
                template.append(linea).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return template.toString();
    }

    private void parsearMenu(Vector <Vector <String>> listaRecetas, String pMayus) {
        String tmp = pMayus.substring(0, 4);

        try {
            if (!listaRecetas.isEmpty()) {
                this.converter.replace("Seccion" + pMayus,
                        "\\section*{\\textit{" + pMayus + "}}\n" +
                                "\\begin{itemize}\n" +
                                "#foreach( $" + tmp + " in $" + pMayus + " )\n" +
                                "\\item[--] $" + tmp + "[0]} \\dotfill $" + tmp + "[1]€\n" +
                                "#end\n" +
                                "\\end{itemize}");

                this.converter.parse(this.template, menuTex);
                this.template = this.leerPlantilla(menuTex);
                this.converter.replace(pMayus, listaRecetas);
            } else {
                this.converter.replace("Seccion" + pMayus, "");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
